import os
import json
import logging
from collections import OrderedDict

from gwpy.segments import Segment

from .. import config
from .. import data

##################################################

def check_observe(event):
    """Observe status at time of lock loss.

    """
    event.rm_tag('OBSERVE')

    gps = event.gps

    window = [-1, 0]
    segment = Segment(*window).shift(gps)

    buf = data.fetch([config.IFO+':GRD-IFO_OK'], segment)[0]

    if bool(buf.data[0]):
        event.add_tag('OBSERVE')
