import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParamsDefault

from gwpy.segments import Segment

from .. import config
from ..event import LocklossEvent
from .. import data

##############################################

def plot_lsc_asc(event):
    gps = event.gps
    plot_windows = config.REFINE_PLOT_WINDOWS
    lsc_asc = config.LSC_ASC_CHANNELS

    # set plot metaparams
    plt.rcParams.update(rcParamsDefault)
    plt.rcParams['font.size'] = 20.0
    plt.rcParams['axes.titlesize'] = 20.0
    plt.rcParams['xtick.labelsize'] = 20.0
    plt.rcParams['ytick.labelsize'] = 20.0
    plt.rcParams['legend.fontsize'] = 20.0
    plt.rcParams['agg.path.chunksize'] = 1000

    for chan_group, channels in lsc_asc.items():
        for window_type, window in plot_windows.items():
            segment = Segment(window).shift(gps)
            bufs = data.fetch(channels, segment)
            for idx, buf in enumerate(bufs):
                color = 'royalblue'
                name = buf.channel
                srate = buf.sample_rate
                t = np.arange(window[0], window[1], 1/srate)
                fig, ax = plt.subplots(1, figsize=(12,8))
                if event.has_tag('REFINED'):
                    status = 'refined'
                else:
                    status = 'unrefined'
                ax.set_xlabel('Time [s] since {} lock loss at {}'.format(status, gps),
                              labelpad=10)
                ax.set_ylabel('Counts')
                ax.set_xlim(window)
                ymin, ymax = calc_ylims(t, buf.data)
                ax.set_ylim(ymin, ymax)
                ax.grid()
                ax.plot(t, buf.data, color = color, label = name, linewidth=2.5)
                ax.legend()

                fig.tight_layout()

                #saves plot to lockloss directory
                outfile_plot = '{}_{}.png'.format(name, window_type)
                outpath_plot = event.gen_path(outfile_plot)
                fig.savefig(outpath_plot, bbox_inches='tight')
                fig.clf()


def calc_ylims(t, y):
    """
    Calculate ylims for the section of SUS channels occuring before lock loss time.
    """

    scaling_times = np.where(t < 0)[0]
    scaling_vals = y[scaling_times]
    ymin, ymax = min(scaling_vals), max(scaling_vals)

    if ymax > 0:
        ymax = 1.1*ymax
    else:
        ymax = 0.9*ymax
    if ymin < 0:
        ymin = 1.1*ymin
    else:
        ymin = 0.9*ymin

    if ymin == ymax:
        ymin, ymax = -0.2, 0.2

    return ymin, ymax


def is_null_channel(data):

    null = None
    if all(data == 0):
        null = True
    else:
        null = False

    return null

###########################################

def _parser_add_arguments(parser):
    parser.add_argument('event', type=int, nargs='?',
                        help="event ID")


def main(args=None):
    """
    Create plots of LSC/ASC channels around a lockloss.
    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    event = LocklossEvent(args.event)
    plot_lsc_asc(event)


if __name__ == '__main__':
    main()
