import os
import logging
import argparse
import numpy as np

from gwpy.segments import Segment

from .. import config
from ..event import LocklossEvent
from .. import data

##################################################

def find_previous_state(event):
    """Collect info about previous guardian state

    Writes out a 'previous_state' file with format:

    previous_state start_gps end_gps lockloss_state

    """
    channels = [config.GRD_STATE_N_CHANNEL]

    # get the lock loss transition itself
    previous_index, lockloss_index = event.transition_index
    state_end = event.transition_gps

    # find lock stretch
    power = 1
    window = config.LOCK_STRETCH_WINDOW

    while True:

        # define search window and search for change in guardian state
        segment = Segment(*window).shift(state_end)
        gbuf = data.fetch(channels, segment)[0]

        transitions = list(data.gen_transitions(gbuf))

        if transitions:
            transition = transitions[-1]
            assert int(transition[2]) == previous_index
            previous_state = (previous_index, transition[0], state_end, lockloss_index)
            logging.info("previous guardian state: {}".format(previous_state))
            break

        window = [edge * (2 ** power) + window[0] for edge in config.LOCK_STRETCH_WINDOW]
        power += 1

    # write start of lock stretch to disk
    path = event.gen_path('previous_state')
    with open(path, 'w') as f:
        f.write('{} {:f} {:f} {}\n'.format(*previous_state))

##################################################

def _parser_add_arguments(parser):
    parser.add_argument('event', type=int, nargs='?',
                        help="event ID")


def main(args=None):
    """
    Find start of lock stretch
    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    event = LocklossEvent(args.event)
    find_lock_start(event)


if __name__ == '__main__':
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    logging.basicConfig(level='DEBUG')
    main()
