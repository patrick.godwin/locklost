from __future__ import print_function, division

import matplotlib
matplotlib.use('Agg')

try:
    from .version import version as __version__
except ImportError:
    __version__ = '?.?.?'
