% for plot_url in plots:
    <div class="col-md-{{size}}">
    <div class="thumbnail">
    <a href="{{plot_url}}" target="_blank"><img src="{{plot_url}}" style="width:100%" class="img-responsive" alt="" /></a>
    </div>
    </div>
    <br />
% end
