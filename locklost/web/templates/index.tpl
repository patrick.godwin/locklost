% rebase('base.tpl', IFO=IFO, web_script=web_script, date=date, online_status=online_status)

% from datetime import timedelta
% from locklost.event import find_events
% from locklost.web import utils

<!-- query info -->
% query_info = ', '.join(['{}={}'.format(k, v) for k, v in query.items()])
<h3>query: {{query_info}}</h3>

<!-- event table -->
<div class="container">
<div class="col-md-12">
<table class="table table-condensed table-hover">
<thead>
<tr>
<th></th>
<th>GPS</th>
<th>UTC</th>
<th>guardian state</th>
<th>state duration</th>
<th>tags</th>
<th>analysis status</th>
</tr>
</thead>
<tbody>

% for ii, event in enumerate(find_events(**query)):
%     tags = utils.tag_buttons(event.list_tags())
%     status_button = utils.analysis_status_button(event)
%     if ii >= query['show']:
%         break
%     end
%     previous_state = event.previous_state
%     if previous_state:
%         previous = previous_state[0]
%         duration = str(timedelta(seconds=int(previous_state[2] - previous_state[1])))
%     else:
%         previous = event.transition_index[0]
%         duration = ''
%     end
      <tr>
      <div class="btn-group">
      <td><span style="color:grey">{{ii}}</span></td>
      <td><a href={{web_script}}/event/{{event.id}}>{{event.gps}}</a></td>
      <td><a href={{web_script}}/event/{{event.id}}>{{event.utc}}</td>
      <td>{{previous}}</td>
      <td>{{duration}}</td>
      <td>{{!tags}}</td>
      <td>{{!status_button}}</td>
      </div>
      </tr>
% end

</tbody>
</table>
</div>
</div>
